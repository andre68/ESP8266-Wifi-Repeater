# ESP8266-Wifi-Repeater
ESP8266 WiFi repeater with ESP-12F (NodeMCU V3) and Arduino IDE.

Any questions? You can ask me directly by my email forpdfsending@gmail.com .

Tutorial is here - http://artfun.pw/post/100.html

How to use it:
1) Download whole repo to your PC.
2) Start IDE and select ESP-12E in boards.
3) Open up WifiRepeater/WifiRepeater.ino in IDE.
4) Change that sketch for your network configuration.
5) Upload sketch, adapted for your network, to esp8266 module (works better with ESP-12F module).
6) Try it.